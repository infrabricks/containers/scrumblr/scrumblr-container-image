# Scrumbler container image 

This dockerfile builds a container for Scrumblr.

## Presentation

"scrumblr is a web-based simulation of a physical agile kanban board that supports real-time collaboration. it is built using node.js, websockets (using socket.io), CSS3, and jquery. i hope you like it."

## Usage

Build command example

```
docker build -t local/scrumbler:latest --progress=plain .
```

## Links

- [Demo](http://scrumblr.ca/)
- [Github repo](https://github.com/aliasaria/scrumblr)
