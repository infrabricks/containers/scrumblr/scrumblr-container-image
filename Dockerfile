# Scrumblr Dockerfile
#
# https://github.com/aliasaria/scrumblr 
# Original author: Ali Asaria
# Adapted by PCLL Team for Eole³ project

##
# Build app
##

FROM node:18-alpine AS builder
ENV NODE_ENV=production
ENV NPM_VERSION=10.8.1
ENV GITREPO_URL="https://github.com/aliasaria/scrumblr.git"

RUN apk add --no-cache git

WORKDIR /src

RUN git clone --depth 1 --branch master \
    -c advice.detachedHead=false ${GITREPO_URL} && \
    cd $(basename -s .git "${GITREPO_URL}") && \
    npm install -g npm@${NPM_VERSION} && npm install && \
    mkdir -p /opt/scrumblr && \
    mv -v client/ node_modules/ lib/ views/ LICENSE.txt server.js /opt/scrumblr

##
# Final image
##

FROM node:18-alpine

ENV SCRUMBLR_PORT="8000"
ENV SCRUMBLR_REDIS_URL="redis-dock"
ENV SCRUMBLR_REDIS_PORT="6379"

LABEL org.opencontainers.image.title="scrumblr" \
    org.opencontainers.image.description="Scrumblr for Docker" \
    org.opencontainers.image.vendor="http://scrumblr.ca/" \
    org.opencontainers.image.documentation="https://docs.joinmobilizon.org" \
    org.opencontainers.image.licenses="GNU GPL-3.0" \
    org.opencontainers.image.source="https://github.com/aliasaria/scrumblr" \
    org.opencontainers.image.maintainer="PCLL Team"

WORKDIR /opt/scrumblr

COPY --from=builder /opt/scrumblr .

CMD node server.js --server:port=${SCRUMBLR_PORT} --redis:url=redis://${SCRUMBLR_REDIS_URL}:${SCRUMBLR_REDIS_PORT}
